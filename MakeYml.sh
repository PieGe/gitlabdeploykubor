#!/bin/bash

echo "apiVersion: v1
kind: Service
metadata:
  name: iris-clusterip-svc
  labels:
    app: iris-clusterip
spec:
  ports:
  - port: 3330
  selector:
    app: iris-clusterip
        
---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: iris-clusterip-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: iris-clusterip
  template:
    metadata:
      labels:
        app: iris-clusterip
    spec:
      containers:
      - name: iris-clusterip-pod
        image: $1
        ports:
        - containerPort: 3330"
